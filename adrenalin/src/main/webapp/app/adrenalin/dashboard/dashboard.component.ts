import { Component, OnInit } from '@angular/core';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'jhi-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(protected dashboardService: DashboardService) {
    dashboardService.find(1);
  }
  ngOnInit(): void {
    this.dashboardService.find(1);
  }
}
