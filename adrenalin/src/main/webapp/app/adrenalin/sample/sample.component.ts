import { Component, OnInit } from '@angular/core';
import { SampleService } from './sample.service';

@Component({
  selector: 'jhi-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss'],
})
export class SampleComponent implements OnInit {
  constructor(protected sampleService: SampleService) {
    sampleService.find(1);
  }

  ngOnInit(): void {
    this.sampleService.find(1);
  }
}
