import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SampleComponent } from './sample.component';

const sampleRoute: Routes = [
  {
    path: '',
    component: SampleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(sampleRoute)],
  exports: [RouterModule]
})
export class SampleRoutingModule {}
