import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { SampleComponent } from './sample.component';
import { SampleRoutingModule } from './sample-routing.module';

@NgModule({
  declarations: [SampleComponent],
  imports: [CommonModule, SharedModule, SampleRoutingModule],
})
export class SampleModule {}
